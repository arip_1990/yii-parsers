<?php

namespace backend\helpers;

class IpHelper
{
    /**
     * @return mixed
     */
    public static function get_real_ip()
    {
        // opredelyaem nastoyaschii ip

        if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) return $_SERVER['HTTP_CF_CONNECTING_IP'];
        elseif ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') return $_SERVER['REMOTE_ADDR'];
        else return false;
    }

    /**
     * @return string
     */
    public static function generateIp(){
        return "" . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255);
    }

}