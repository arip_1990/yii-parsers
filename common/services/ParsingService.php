<?php

namespace common\services;

use common\models\ParserModel;
use common\repositories\read\ShopsReadRepositories;
use console\extended\checkDirectory;
use console\interfaces\ShopGeneralInterface;
use Yii;
use yii\caching\Cache;

/**
 * Class ParsingService
 * @package common\services
 * @var $parserModel ParserModel
 * @var ParserModel[] $allListInfo
 * @var Cache $cache
 * @var ShopsReadRepositories $shopReadRepositories
 */
class ParsingService
{
    private $cache;

    private $shopReadRepositories;


    /**
     * ParsingService constructor.
     * @param Cache $cache
     * @param checkDirectory $checkDirectory
     * @param ShopsReadRepositories $shopReadRepositories
     */
    public function __construct(Cache $cache,
                                checkDirectory $checkDirectory,
                                ShopsReadRepositories $shopReadRepositories)
    {
        //$this->parserModel = new ParserModel($cache, $checkDirectory);
        $this->cache = $cache;
        $this->shopReadRepositories = $shopReadRepositories;
    }


    public function scanForList(ShopGeneralInterface $shopGeneral)
    {
        $shopGeneral->parseForList();
    }

    public function parseProduct(ShopGeneralInterface $shopGeneral)
    {
        $shopGeneral->parseProduct();
    }

}