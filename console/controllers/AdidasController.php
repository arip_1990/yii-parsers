<?php

namespace console\controllers;

use backend\helpers\IpHelper;
use backend\helpers\StringHelper;
use backend\models\Process;
use common\models\Query;
use common\models\Settings;
use common\models\Shops;
use common\repositories\read\ShopsReadRepositories;
use common\services\ParsingService;
use console\extended\checkDirectory;
use console\interfaces\ShopInterface;
use console\repositories\mailRepositorie\sendMail;
use console\shops\Adidas;
use console\shops\GeneralShopClass;
use Yii;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\db\Connection;

/**
 * Class AdidasController
 * @package console\controllers
 * @property  \yii\db\Connection $connection
 */
class AdidasController extends Controller //____ berem vse dannie na Nike i vstavlyaem v BD
{
    const NAME_ADIDAS_MEN = 'adidas_men';
    const NAME_ADIDAS_WOMEN = 'adidas_women';
    const NAME_ADIDAS_GARCONS = 'adidas_garcons';
    const NAME_ADIDAS_FILLES = 'adidas_filles';

    private $checkDirectory;
    private $mailer;

    private $tableau_sex_ass;
    private $parsingService;


    /**
     * NikeController constructor
     * @param string $id
     * @param $module
     * @param sendMail $mailer
     * @param checkDirectory $checkDirectory
     * @param ParsingService $parsingService
     * @param array $config
     */
    public function __construct(string $id, $module,
                                sendMail $mailer,
                                checkDirectory $checkDirectory,
                                ParsingService $parsingService,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->checkDirectory = $checkDirectory;
        $this->parsingService = $parsingService;
        $this->mailer = $mailer;

        $this->tableau_sex_ass = [
            self::NAME_ADIDAS_MEN => 0,
            self::NAME_ADIDAS_WOMEN => 1,
            self::NAME_ADIDAS_GARCONS => 2,
            self::NAME_ADIDAS_FILLES => 3
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \Exception
     */
    public function beforeAction($action)
    {
        if (!$this->checkParseOrNot()) { // __ Proveryaem admin dal li razreshenie
            return false;
        }
        return parent::beforeAction($action);
    }

    /**
     * @var  yii\db\Connection $db_root
     * @return bool
     */
    private function checkParseOrNot()
    {
        $db_root = Yii::$app->db_root;
        $settings = $db_root->createCommand('SELECT * FROM settings')->queryOne();
        return $settings['image_parse_on_off'];
    }



    /**
     * @throws \Exception
     */
    public function actionMakeListMen()
    {
        $this->scanForList(
            'https://www.adidas.fr/chaussures-hommes?start=',
            self::NAME_ADIDAS_MEN);
    }


    /**
     * @throws \Exception
     */
    public function actionMakeListWomen()
    {
        $this->scanForList(
            'https://www.adidas.fr/chaussures-femmes?start=',
            self::NAME_ADIDAS_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListGarcons()
    {
        $this->scanForList(
            'https://www.adidas.fr/garcons-adolescents_8_16_ans?start=',
            self::NAME_ADIDAS_GARCONS);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListFilles()
    {
        $this->scanForList(
            'https://www.adidas.fr/filles-adolescents_8_16_ans?start=',
            self::NAME_ADIDAS_FILLES);
    }


    /**
     * @throws \Exception
     */
    public function actionParsePageMen()
    {
        $this->parsePage(self::NAME_ADIDAS_MEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageWomen()
    {
        $this->parsePage(self::NAME_ADIDAS_WOMEN);
    }

    /**
     * @param $category
     * @return void
     */
    private function parsePage($category)
    {
        $adidas = new Adidas($this->mailer);
        $adidas->category = $category;
        $this->parsingService->parseProduct($adidas);
    }


    /**
     * @param $url //__ $url dmya parsinga lista
     * @param $category //__ $category (men, women m....)
     */
    private function scanForList($url, $category)
    {
        $adidas = new Adidas($this->mailer);
        $adidas->url = $url;
        $adidas->console_web = GeneralShopClass::LAUNCH_WEB;
        $adidas->category = $category;
        $this->parsingService->scanForList($adidas);
    }






}