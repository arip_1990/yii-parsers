<?php

namespace console\controllers;

use common\repositories\read\ShopsReadRepositories;
use common\services\ParsingService;
use console\extended\checkDirectory;
use console\helpers\Utils;
use console\repositories\mailRepositorie\sendMail;
use console\shops\GeneralShopClass;
use console\shops\Nike;
use Yii;
use yii\base\Action;
use yii\console\Controller;

/**
 * Class NikeController
 * @package console\controllers
 * @property  yii\db\Connection $connection
 */
class NikeController extends Controller
{
    const NAME_NIKE_MEN = 'nike_men';
    const NAME_NIKE_WOMEN = 'nike_women';
    const NAME_NIKE_GARCONES = 'nike_garcons';
    const NAME_NIKE_FILLES = 'nike_filles';

    private $checkDirectory;
    private $mailer;
    private $shopReadRepositories;
    private $parsingService;

    /**
     * NikeController constructor.
     * @param string $id
     * @param $module
     * @param sendMail $mailer
     * @param checkDirectory $checkDirectory
     * @param ShopsReadRepositories $shopReadRepositories
     * @param ParsingService $parsingService
     * @param array $config
     */
    public function __construct(string $id, $module,
                                sendMail $mailer,
                                checkDirectory $checkDirectory,
                                ShopsReadRepositories $shopReadRepositories,
                                ParsingService $parsingService,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->checkDirectory = $checkDirectory;
        $this->shopReadRepositories = $shopReadRepositories;
        $this->parsingService = $parsingService;
        $this->mailer = $mailer;
    }

    /**
     * @param Action $action
     * @return bool
     * @throws \Exception
     */
    public function beforeAction($action) {
        if (!Utils::checkParseOrNot())
            return false;
        return parent::beforeAction($action);
    }

    /**
     * @throws \Exception
     */
    public function actionInsert()
    {
        $this->checkDirectory->init('nike');
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListMen()
    {
        $this->scanForList(
            'https://store.nike.com/html-services/gridwallData?country=FR&lang_locale=fr_FR&gridwallPath=homme-chaussures/7puZoi3&pn=',
            self::NAME_NIKE_MEN);
    }


    /**
     * @throws \Exception
     */
    public function actionMakeListWomen()
    {
        $this->scanForList(
            'https://store.nike.com/html-services/gridwallData?country=FR&lang_locale=fr_FR&gridwallPath=femme-chaussures/7ptZoi3&pn=',
            self::NAME_NIKE_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListGarcons()
    {
        $this->scanForList(
            'https://store.nike.com/html-services/gridwallData?country=FR&lang_locale=fr_FR&gridwallPath=garçon-chaussures/7pvZoi3&pn=',
            self::NAME_NIKE_GARCONES);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListFilles()
    {
        $this->scanForList(
            'https://store.nike.com/html-services/gridwallData?country=FR&lang_locale=fr_FR&gridwallPath=girls-shoes7pwZoi3&pn=',
            self::NAME_NIKE_FILLES);
    }


    /**
     * @throws \Exception
     */
    public function actionParsePageMen()
    {
        $this->parsePage(self::NAME_NIKE_MEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageWomen()
    {
        $this->parsePage(self::NAME_NIKE_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageGarcons()
    {
        $this->parsePage(self::NAME_NIKE_GARCONES);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageFilles()
    {
        $this->parsePage(self::NAME_NIKE_FILLES);
    }

    /**
     * @param $category
     */
    private function parsePage($category)
    {
        $nike = new Nike($this->mailer);
        $nike->category = $category;
        $this->parsingService->parseProduct($nike);
    }


    /**
     * @param $url //__ $url dmya parsinga lista
     * @param $category //__ $category (men, women m....)
     */
    private function scanForList($url, $category)
    {
        $nike = new Nike($this->mailer);
        $nike->url = $url;
        $nike->console_web = GeneralShopClass::LAUNCH_WEB;
        $nike->category = $category;
        $this->parsingService->scanForList($nike);
    }
}