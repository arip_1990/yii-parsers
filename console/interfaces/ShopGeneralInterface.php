<?php

namespace console\interfaces;

interface ShopGeneralInterface
{
    /**
     * @return mixed
     */
    public function parseForList();

    /**
     * @return mixed
     */
    public function parseProduct();
}


