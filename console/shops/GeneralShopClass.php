<?php

namespace console\shops;

use common\repositories\read\ShopsReadRepositories;
use console\extended\checkDirectory;
use console\repositories\mailRepositorie\sendMail;

/**
 * Class GeneralShopClass
 * @package console\shops
 * @property  int $last_product_id
 * @property  int $start_product_id
 * @property  ShopsReadRepositories $shopReadRepositories
 * @property  int $got_last_product_id
 * @property  string $path_sql_path
 * @property  string $_name_directory
 * @property  checkDirectory $checkDirectory
 */
class GeneralShopClass
{
    const LAUNCH_CONSOLE = 1;
    const LAUNCH_WEB = 0;

    public $last_product_id;
    public $start_product_id;
    public $shopReadRepositories;
    public $got_last_product_id;
    public $path_sql_path;
    public $_name_directory;
    public $checkDirectory;
    public $tableau_sex_ass;
    public $console_web;
    public $url;
    public $category;

    /**
     * GeneralShopClass constructor.
     * @param sendMail $mailer
     */
    public function __construct(sendMail $mailer)
    {
        $this->shopReadRepositories = new  ShopsReadRepositories();
        $this->checkDirectory = new checkDirectory($mailer);
    }
}